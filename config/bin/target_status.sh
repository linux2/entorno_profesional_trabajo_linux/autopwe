#!/bin/sh

target_ip=$(cat /home/esb/.config/bin/target | awk '{print $1}')
target_name=$(cat /home/esb/.config/bin/target | awk '{print $2}')

if [ $target_ip ] && [ $target_name ]; then
    echo "%{F#ff0a0a}什 %{F#ffffff}$target_ip%{u-} - $target_name"
else
    echo "%{F#ff0a0a}什 %{F#ffffff} No target"
fi
