#!/bin/bash

# Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
cyan="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

# User paths
userPath=/home/esb
rootPath=/root
cwd=$(pwd)

# Banner
function banner(){
    # https://patorjk.com/software/taag/#p=display&h=0&f=Katakana&t=Auto-PWE
    echo -e "

              d8888          888                    8888888b.  888       888 8888888888    ${cyan}Auto Professional Working Environment${end}
             d88888          888                    888   Y88b 888   o   888 888           
            d88P888          888                    888    888 888  d8b  888 888           ${yellow}[*]${end} Script to automatically install your customized Parrot working environment.
           d88P 888 888  888 888888  .d88b.         888   d88P 888 d888b 888 8888888       ${yellow}[*]${end} Inspired by ${red}S4vitar${end}
          d88P  888 888  888 888    d88\"\"88b        8888888P\"  888d88888b888 888           ${yellow}[*]${end} Video by S4vitar: https://www.youtube.com/watch?v=mHLwfI1nHHY
         d88P   888 888  888 888    888  888 888888 888        88888P Y88888 888        
        d8888888888 Y88b 888 Y88b.  Y88..88P        888        8888P   Y8888 888           Created by ${blue}ESB81${end}
       d88P     888  \"Y88888 \"Y888   \"Y88P\"	    888        888P     Y888 8888888888 

    "
}


#############
# FUNCTIONS #############################################################################################################
#############


# System Update & Upgrade
function sys_update(){
    apt-get update && parrot-upgrade -y
}

# Install all the required packages
function reqs(){
    
    # General purpose packages:
    apt install build-essential git vim xcb libxcb-util0-dev libxcb-ewmh-dev libxcb-randr0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-xinerama0-dev libasound2-dev libxcb-xtest0-dev libxcb-shape0-dev -y

    # Polybar related packages:
    apt install cmake cmake-data pkg-config python3-sphinx libcairo2-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev python3-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-xkb-dev libxcb-xrm-dev libxcb-cursor-dev libasound2-dev libpulse-dev libjsoncpp-dev libmpdclient-dev libcurl4-openssl-dev libnl-genl-3-dev -y

    # Picom related packages:
    apt install meson libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev libpcre2-dev libevdev-dev uthash-dev libev-dev libx11-xcb-dev libxcb-glx0-dev -y

    # Rofi:
    apt install rofi -y

    # Firejail:
    apt install firejail -y

    # feh:
    apt install feh -y

    # BSPWM
    apt install bspwm -y
}


# BSPWM installation and configuration
function bspwm_install(){
    
    # Installation
    cd $cwd
    git clone https://github.com/baskerville/bspwm.git
    cd bspwm/
    make
    make install
    apt install bspwm -y
    
    # Configuration
    mkdir $userPath/.config/bspwm
    cd $userPath/.config/bspwm
    cp $cwd/config/bspwm/bspwmrc .
    chmod +x bspwmrc
}


# SXHKD installation and configuration
function sxhkd_install(){
    
    # Installation
    cd $cwd
    git clone https://github.com/baskerville/sxhkd.git
    cd sxhkd/

    # Configuration
    mkdir $userPath/.config/sxhkd
    cd $userPath/.config/sxhkd/
    cp $cwd/config/sxhkd/sxhkdrc .
    chmod +x sxhkdrc

    # Create bwpsm_resize
    mkdir $userPath/.config/bspwm/scripts/
    cd $userPath/.config/bspwm/scripts
    cp $cwd/config/bspwm/scripts/bspwm_resize .
    chmod +x bspwm_resize
}

# Polybar installation
function polybar_install(){
    
    # Installation
    cd $cwd
    git clone --recursive https://github.com/polybar/polybar
    cd polybar/
    mkdir build
    cd build/
    cmake ..
    make -j$(nproc)
    make install

    # Blue-Sky configuration and fonts
    cd $cwd
    git clone https://github.com/VaughnValle/blue-sky.git
    mkdir $userPath/.config/polybar
    cd blue-sky/polybar/
    cp * -r $userPath/.config/polybar
    cd fonts
    cp * /usr/share/fonts/truetype/
    fc-cache -v

    # My custom configuration
    cd $userPath/.config/polybar
    cp $cwd/config/polybar/current.ini .
    cp $cwd/config/polybar/launch.sh .
    chmod +x current.ini launch.sh

    mkdir $userPath/.config/bin
    cd $userPath/.config/bin
    cp $cwd/config/bin/*.sh .
    cp $cwd/config/bin/target .
    chmod +x *
}

# Picom installation
function picom_install(){
    
    # Installation
    cd $cwd
    git clone https://github.com/ibhagwan/picom.git
    cd picom/
    git submodule update --init --recursive
    meson --buildtype=release . build
    ninja -C build
    ninja -C build install

    # Configuration
    mkdir $userPath/.config/picom
    cd $userPath/.config/picom
    cp $cwd/config/picom/picom.conf .
}

# HackNerd fonts installation
function hacknerdfonts_install(){
    cd $cwd
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip
    unzip Hack.zip
    cp *.ttf /usr/local/share/fonts/
    fc-cache -v  
}

# Firefox installation
function firefox_install(){
    cd $cwd
    wget -O firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=es-ES"
    tar -xf firefox.tar.bz2
    mv firefox/ /opt/
}

# Wallpapers installation
function wallpapers_install(){
    mkdir $userPath/wallpapers
    cd $userPath/wallpapers
    wget https://gitlab.com/linux2/entorno_profesional_trabajo_linux/autopwe/-/raw/main/wallpapers/espacio_wallpaper_2021.jpg
    wget https://gitlab.com/linux2/entorno_profesional_trabajo_linux/autopwe/-/raw/main/wallpapers/jarvis_wallpaper_2021.jpg
    wget https://gitlab.com/linux2/entorno_profesional_trabajo_linux/autopwe/-/raw/main/wallpapers/s4vitar_wallpaper_2020.jpg
}

# Rofi "Nord" theme configuration
function rofi_config(){
    mkdir -p $userPath/.config/rofi/themes
    cp $cwd/blue-sky/nord.rasi $userPath/.config/rofi/themes
    cp $cwd/config/rofi/config $userPath/.config/rofi
}

# Nano configuration
function nano_config(){
    cp $cwd/nanorc $userPath/.nanorc
}

#Powerlevel10K and ZSH related installation and configuration
function p10k_zsh_installation(){
    
    # P10K for "esb" user:
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $userPath/powerlevel10k
    cp $cwd/zshrc $userPath/.zshrc
    cp $cwd/p10k.zsh $userPath/.p10k.zsh

    # P10K for "root" user:
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $rootPath/powerlevel10k
    cp $cwd/p10k.zsh.root $rootPath/.p10k.zsh

    # Symbolic link of "zshrc" file for "root" user:
    ln -s -f $userPath/.zshrc $rootPath/.zshrc
    
    # Set default shell to "zsh" for "esb" and "root" users:
    usermod --shell /usr/bin/zsh esb
    usermod --shell /usr/bin/zsh root
    
    # Minor fix about problems with permissions when migrating to a user from "root" through "su" command:
    chown esb:esb /root
    chown esb:esb /root/.cache -R
    chown esb:esb /root/.local -R
}

# {zsh-syntax-highlighting zsh-autocomplete zsh-autosuggestions} plugins installation
function zsh_plugins_installation(){
    cd $cwd
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git 
    git clone https://github.com/marlonrichert/zsh-autocomplete.git 
    git clone https://github.com/zsh-users/zsh-autosuggestions

    sudo mv zsh-syntax-highlighting /usr/share
    sudo mv zsh-autocomplete /usr/share
    sudo mv zsh-autosuggestions /usr/share
}

# BAT installation
function bat_install(){
    cd $cwd
    wget https://github.com/sharkdp/bat/releases/download/v0.18.2/bat_0.18.2_amd64.deb
    sudo dpkg -i bat_0.18.2_amd64.deb    
}

# LSD installation
function lsd_install(){
    cd $cwd
    wget https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd_0.20.1_amd64.deb
    sudo dpkg -i lsd_0.20.1_amd64.deb    
}

# EXA installation
function exa_install(){
    cd $cwd
    wget https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip
    unzip exa-linux-x86_64-v*.zip -d exa
    sudo mv exa/bin/exa /usr/local/bin/
}

# FZF installation
function fzf_install(){
    
    # For "esb" user:
    git clone --depth 1 https://github.com/junegunn/fzf.git $userPath/.fzf
    chown -R esb:esb $userPath/.fzf
    su esb -c "$userPath/.fzf/install"
    
    # For "root" user:
    git clone --depth 1 https://github.com/junegunn/fzf.git $rootPath/.fzf
    $rootPath/.fzf/install
}

# Oh-My-Tmux installation
function omt_install(){
    # For "esb" user:
    cd $userPath
    git clone https://github.com/gpakosz/.tmux.git
    ln -s -f .tmux/.tmux.conf
    cp .tmux/.tmux.conf.local .
    
    # For "root" user:
    git clone https://github.com/gpakosz/.tmux.git $rootPath/.tmux
    ln -s -f $rootPath/.tmux/.tmux.conf $rootPath/.tmux.conf
    cp $rootPath/.tmux/.tmux.conf.local $rootPath/
}

# "WhichSystem.py" program installation
function whichsystem_install(){
    cd $cwd
    cp scripts/whichSystem.py /usr/bin/whichSystem.py
    chmod +x /usr/bin/whichSystem.py
}

# "FastTCPScan.go" program installation
function fasttcpscan_install(){
    cd $cwd
    cp scripts/fastTCPScan /usr/bin/fastTCPScan
    chmod +x /usr/bin/fastTCPScan
}

# Copy HowTos folder
function howtos(){
    cd $cwd
    cp -R 0-HowTos $userPath
}

# Set ownership of "esb" user folders and files
function ownership_config(){
    cd $userPath
    chown -R esb:esb .tmux/ .tmux.conf.local .tmux.conf .p10k.zsh .nanorc wallpapers/ powerlevel10k/ .config/ 0-HowTos/
}

# htbExplorer install
function htbExplorer_install(){
    cd $userPath
    git clone https://github.com/s4vitar/htbExplorer.git
}


########
# MAIN #############################################################################################################
########

## git clone https://github.com/jmlgomez73/Shockz-MKE && cd Shockz-MKE && chmod +x install.sh && ./install.sh


# Check user privileges
[ "$(id -u)" -ne 0 ] && (echo -e "${red} [!] This script must be run as root ${end}" >&2) && exit 1;

# Show banner
banner

# System update & upgrade
sys_update

# Install all the required packages
reqs

# BSPWM and SXHKD installation and configuration
bspwm_install
sxhkd_install

# Polybar installation
polybar_install

# Picom installation
picom_install

# HackNerd Fonts installation
hacknerdfonts_install

# Firefox installation
firefox_install

# Wallpapers installation
wallpapers_install

# Rofi "Nord" theme configuration
rofi_config

# Nano configuration
nano_config

# Powerlevel10K and ZSH related configuration
p10k_zsh_installation

# {zsh-syntax-highlighting zsh-autocomplete zsh-autosuggestions} plugins installation
zsh_plugins_installation

# BAT installation
bat_install

# LSD installation
lsd_install

# EXA installation
exa_install

# FZF installation
fzf_install

# Oh-my-tmux installation
omt_install

# "WhichSystem.py" program installation
whichsystem_install

# "FastTCPScan.go" program installation
fasttcpscan_install

# Copy HowTos folder
howtos

# Set ownershipt of "esb" folders and files
ownership_config

# htbExplorer
htbExplorer_install

# Installation Finished.Post-Installation steps:
echo -e "\n${yellow}[*]${end} Installation finished!"
echo -e "${yellow}[*]${end} After system reboot, please consider performing the following steps: "
echo -e "\t${yellow}1)${end} Terminal related configuration (HackNerd fonts, customization, etc.)"
echo -e "\t${yellow}2)${end} \"FoxyProxy\" Firefox plugin installation."
echo -e "\t${yellow}3)${end} \"htbExplorer\" API configuration."

# System Reboot
echo -e "\n${yellow}[*]${end} System reboot in 10 seconds..."
sleep 10
reboot
