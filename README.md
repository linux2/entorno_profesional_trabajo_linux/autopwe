# AutoPWE

Script que realiza la instalación automática del "Entorno profesional de trabajo de Parrot" descrito por S4vitar en la versión de 2021.

* **2021**
  * [CONVIERTE TU LINUX EN UN ENTORNO PROFESIONAL DE TRABAJO](https://www.youtube.com/watch?v=mHLwfI1nHHY&t=0s) 

## Installation

1. **Cambiamos al usuario root:**

      ```zsh
      $ sudo su
      ```

2. **Clonamos el repositorio, damos permisos de ejecución al archivo "install.sh" y lo ejecutamos:**

      ```zsh
      $ git clone https://gitlab.com/linux2/entorno_profesional_trabajo_linux/autopwe.git && cd autopwe && chmod +x install.sh && ./install.sh
      ```

3. **Seleccionar como lanzador bspwm:**

      Una vez el instalador termine reiniciará el sistema y en la pantalla de Login tendremos que seleccionar "BSPWM".


## Post-Installation Steps

1) Modificar las preferencias del gnome-terminal:

     * **Pestaña "General"**:
	     * Quitar el "tick" de "Usar la tipografía de ancho fijo del sistema" y seleccionar: Hack Nerd Font Mono Regular, con tamaño 10.
	     * Quitar el "tick" de "Mostrar la barra de menús en los terminales nuevos por defecto.
	     * Quitar el "tick" de "Campana de terminal".
		 * Seleccionar "Forma del cursor": doble T
	 * **Pestaña "Desplazamiento"**:
	     * Modificar la opción "La barra de desplazamiento está" a la opción: Desconectado.

2) Firefox:

   - Instalamos el addon 'FoxyProxy' para Firefox.
   - Configuramos la privacidad en Firefox y el directorio de descargas principal.

   
3) Tanto para el usuario "esb" como para "root", las opciones a elegir al cambiar a ZSH son las siguientes:

	```zsh
	$ p10k configure
	```

     1. y
     2. y
	 3. y
	 4. y
	 5. 2-classic
	 6. 1-Unicode
	 7. 3-Dark
	 8. No
	 9. 1-Angled
	 10. 2-Blurred
	 11. 2-Blurred
	 12. 1-One Lnie
	 13. 2-Sparse
	 14. 2-Many Icons
	 15. 2-Fluent
	 16. y
	 17. 2-Quiet
	 18. y
