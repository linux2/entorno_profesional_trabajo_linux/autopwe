# Colours
greenColour="\e[0;32m\033[1m"
endColour="\033[0m\e[0m"
redColour="\e[0;31m\033[1m"
blueColour="\e[0;34m\033[1m"
yellowColour="\e[0;33m\033[1m"
purpleColour="\e[0;35m\033[1m"
turquoiseColour="\e[0;36m\033[1m"
grayColour="\e[0;37m\033[1m"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Prompt
PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"
# Export PATH$
export PATH=~/.local/bin:/snap/bin:/usr/sandbox/:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/usr/local/sbin:/usr/sbin:/sbin:$PATH

# Fix the Java Problem
export _JAVA_AWK_WM_NONREPARENTING=1

#####################################################
# Auto completion / suggestion
# Mixing zsh-autocomplete and zsh-autosuggestions
# Requires: zsh-autocomplete (custom packaging by Parrot Team)
# Jobs: suggest files / foldername / histsory bellow the prompt
# Requires: zsh-autosuggestions (packaging by Debian Team)
# Jobs: Fish-like suggestion for command history
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-autocomplete/zsh-autocomplete.plugin.zsh
# Select all suggestion instead of top on result only
zstyle ':autocomplete:tab:*' insert-unambiguous yes
zstyle ':autocomplete:tab:*' widget-style menu-select
zstyle ':autocomplete:*' min-input 2
bindkey $key[Up] up-line-or-history
bindkey $key[Down] down-line-or-history
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char

##################################################
# Fish like syntax highlighting
# Requires "zsh-syntax-highlighting" from apt

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Save type history for completion and easier life
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt histignorealldups sharehistory
#setopt appendhistory

# Useful alias for benchmarking programs
# require install package "time" sudo apt install time
# alias time="/usr/bin/time -f '\t%E real,\t%U user,\t%S sys,\t%K amem,\t%M mmem'"
# Display last command interminal
echo -en "\e]2;Parrot Terminal\a"
preexec () { print -Pn "\e]0;$1 - Parrot Terminal\a" }

source ~/powerlevel10k/powerlevel10k.zsh-theme

#########
# ALIAS ########################################################################
#########

#alias ls='ls -lh --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Custom ESB Aliases

alias ..='cd ..'
alias c='clear'
alias cat='bat'
alias catn='/bin/cat'
alias catnl='/bin/bat --paging=never'
alias esb_shortcuts='less /home/esb/0-HowTos/0-Atajos_de_teclado.txt'
alias ll='lsd -lh --group-dirs=first'
alias la='lsd -a --group-dirs=first'
alias l='lsd --group-dirs=first'
alias ls='lsd -lh --group-dirs=first'
alias misfunciones='more +120 /home/esb/.zshrc'   
alias misalias='more +64 /home/esb/.zshrc'
alias poweroff='sudo poweroff'
alias reload='source ~/.zshrc'
alias reboot='sudo reboot'
alias tty='/home/esb/.config/bin/tty.sh'
alias update='sudo apt-get update'
alias upgrade='sudo apt-get upgrade'

# Alias Pentesting

alias howto_preparativos_tmux='less /home/esb/0-HowTos/1-Pentesting/0-Preparativos_TMUX.txt'
alias howto_osint='less /home/esb/0-HowTos/1-Pentesting/1-OSINT.txt'
alias howto_recon-enum='less /home/esb/0-HowTos/1-Pentesting/2-Reconocimiento_Enumeration.txt'
alias howto_vuln-scan='less /home/esb/0-HowTos/1-Pentesting/3-Vulnerability_Scan.txt'
alias howto_web-attacks='less /home/esb/0-HowTos/1-Pentesting/4-Web_Attacks.txt'
alias howto_network-attacks='less /home/esb/0-HowTos/1-Pentesting/5-Network_Attacks.txt'
alias howto_reverse-shells='less /home/esb/0-HowTos/1-Pentesting/6-Reverse_Shells.txt'
alias howto_searchsploit='less /home/esb/0-HowTos/1-Pentesting/7-Searchsploit.txt'
alias howto_tratamiento_TTY='less /home/esb/0-HowTos/1-Pentesting/8-Tratamiento_TTY.txt'
alias howto_wifi_pentesting='less /home/esb/0-HowTos/1-Pentesting/9-Wi-Fi_Pentesting.txt'
alias howto_burpsuite='less /home/esb/0-HowTos/1-Pentesting/A-Burpsuite.txt'


# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ $KEYMAP == vicmd ]] || [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ $KEYMAP == main ]] || [[ $KEYMAP == viins ]] || [[ $KEYMAP = '' ]] || [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

# Start with beam shape cursor on zsh startup and after every command.
zle-line-init() { zle-keymap-select 'beam'}

########################
# CUSTOM ESB FUNCTIONS ##################################################
########################

# Lista de howtos relativos al pentesting
function esb_hacking_howtos(){

	echo -e "\n 0) ESB Shortcuts: \t\t\t >esb_shortcuts"
    echo -e " 1) Preparativos TMUX: \t\t\t >howto_preparativos_tmux"
    echo -e " 2) OSINT: \t\t\t\t >howto_osint"
    echo -e " 3) Reconnaissance - Enumeration: \t >howto_recon-enum"
    echo -e " 4) Vulnerability Scan: \t\t >howto_vuln-scan"
    echo -e " 5) Web Attacks: \t\t\t >howto_web-attacks"
    echo -e " 6) Network Attacks: \t\t\t >howto_network-attacks"
    echo -e " 7) Reverse Shells: \t\t\t >howto_reverse-shells"
    echo -e " 8) Searchsploit: \t\t\t >howto_searchsploit"
    echo -e " 9) Tratamiento TTY: \t\t\t >howto_tratamiento_tty"
    echo -e " 10) Wi-Fi Pentesting: \t\t\t >howto_wifi_pentesting\n"
    echo -e " =================================================================== \n"
    echo -e " A) Burpsuite: \t\t\t\t >howto_burpsuite\n"
}

# Make tree work directory structure
function mkt(){
    mkdir {nmap,content,scripts,tmp,exploits}
}

# Extract port from "allPorts" file from nmap
function extractPorts(){
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"

    ip_address=$(cat allPorts | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u)
    open_ports=$(cat allPorts | grep -oP '\d{1,5}/open' | awk '{print $1}' FS="/"| xargs | tr ' ' ',')

    echo -e "\t${blueColour}[*] IP Address: ${endColour}${grayColour}$ip_address${endColour}"
    echo -e "\t${blueColour}[*] Open Ports: ${endColour}${grayColour}$open_ports${endColour}\n"

    echo $open_ports | tr -d "\n" | xclip -sel clip

    echo  -e "${yellowColour}[*] Ports have been copied to clipboard!${endColour}\n"
}

# Extract IP from "allIP" file from "hostDiscovery.sh"
function extractIPs(){
    echo -e "\n${yellowColour}[*] Extracting IP addresses...${endColour}\n"

    ip_address=$(cat allIP | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | xargs)

    echo -e "\t${blueColour}[*] IP Address: ${endColour}${grayColour}$ip_address${endColour}"

    echo $ip_address | tr -d "\n" | xclip -sel clip

    echo  -e "${yellowColour}[*] IPs have been copied to clipboard!${endColour}\n"
}

# Eliminación profunda de archivos
function rmk(){
	scrub -p dod $1
	shred -zun 10 -v $1
}

# Definir el "target" para el bloque de la barra superior
function settarget(){
    target_ip=$1
    target_name=$2
    echo "$target_ip $target_name" > ~/.config/bin/target
}

# Eliminar/varcias el valor del bloque "target" de la barra superior
function cleartarget(){
    echo '' > ~/.config/bin/target
}

# Golang vars
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$HOME/.local/bin:$PATH
