#!/bin/bash

ip=`echo $1 | awk -F'.' '{ print $1,$2,$3 }' OFS='.'`

for i in $(seq 2 254); do
    timeout 1 bash -c "ping -c 1 $ip.$i > /dev/null 2>&1" && echo "Host $ip.$i - ACTIVE" && echo "$ip.$i" >> allIP &
done; wait
