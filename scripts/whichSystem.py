#!/usr/bin/python3
#coding: utf-8

import re, sys, subprocess

# Regular expression for validating an IP-Address:
regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"

def usage():
    print("[*] Usage: python3 " + sys.argv[0] + " <IP_Address>\n")

def check_input():
    if len(sys.argv) != 2:
        usage()
        sys.exit(1)

    if not (re.search(regex, sys.argv[1])):
        print("\n[*] Invalid IP address!\n")
        usage()
        sys.exit(1)

def get_ttl(ip_address):
    proc = subprocess.Popen(["/usr/bin/ping -c 1 %s" % ip_address, ""], stdout=subprocess.PIPE, shell=True)
    (out,err) = proc.communicate()

    out = out.split()
    out = out[12].decode('utf-8')

    ttl_value = re.findall(r"\d{1,3}", out)[0]

    return(ttl_value)

def get_os(ttl):

    ttl = int(ttl)

    if ttl >= 0 and ttl <= 64:
        return "Linux"
    elif ttl >= 65 and ttl <= 128:
        return "Windows"
    else:
        return "Not found"

if __name__ == '__main__':

    check_input()

    ip_address = sys.argv[1]
    ttl = get_ttl(ip_address)

    os_name = get_os(ttl)
    print("\n%s (TTL -> %s): %s\n" % (ip_address, ttl, os_name))
